'use strict';

/**
 * @ngdoc function
 * @name countryPortfolioApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the countryPortfolioApp
 */
angular.module('countryPortfolioApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
