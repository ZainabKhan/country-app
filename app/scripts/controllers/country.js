'use strict';

/**
 * @ngdoc function
 * @name countryPortfolioApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the countryPortfolioApp
 */

angular.module('countryPortfolioApp')
	.factory('countryService', ['$http', '$q', function($http, $q) {
		var countries = [];
		var last_request_failed = true;
		var promise = undefined;

		return {
			getCountries: function() {
				if (!promise || last_request_failed) {
					promise = $http.get('https://restcountries.eu/rest/v1/all').then(
						function(response) {
							last_request_failed = false;
							countries = response.data;
							return countries;
						},
						function(response) { // error
							last_request_failed = true;
							return $q.reject(response);
						});
				}
				return promise;
			},
		};
	}])

	.controller('CountryCtrl', function(countryService, $scope, $q, $http, $routeParams) {
		var countryApiCall = $http.get('https://restcountries.eu/rest/v1/all', { cache: true });

		$scope.countries = null;
		$scope.currentCountry = null;

		$scope.getCountries = function() {
			countryService
				.getCountries()
				.then(function function_name (countries) {
					$scope.countries = countries;
				});
		}

		$scope.countryDetail = function() {
			countryService
				.getCountries()
				.then(function(countries) {
					countries.forEach(function(country) {
						if (country.alpha2Code === $routeParams.code) {
							$scope.currentCountry = country;
						}
				});
			});
		}
	});
