'use strict';

/**
 * @ngdoc overview
 * @name countryPortfolioApp
 * @description
 * # countryPortfolioApp
 *
 * Main module of the application.
 */
angular
  .module('countryPortfolioApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/countries', {
        templateUrl: 'views/countries.html',
        controller: 'CountryCtrl',
        controllerAs: 'country'
      })
      .when('/countries/:code', {
        templateUrl: 'views/country-detail.html',
        controller: 'CountryCtrl',
        controllerAs: 'country'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
